%define ver 0.0.7
%define rel 1

Summary: A tool for determining versions of installed GNOME packages.
Name: pkgview
Version: %{ver}
Release: %{rel}
License: GPL
Group: Applications/System
Source: http://www.gtnorthern,demon.co.uk/packages/pkgview/pkgview-%{ver}.tar.gz
BuildRoot: /var/tmp/%{name}-%{version}-root
Vendor: Principia Systems
Packager: Mike Newman <packages@principia-systems.co.uk>

%define prefix /usr
%define sysconfdir /etc

%description
Displays version information for desktop components, and determines the
overall desktop version from the gnome-version.xml file.

%prep
%setup

%build
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh $ARCH_FLAGS --prefix=%{prefix} --sysconfdir=%{sysconfdir}
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure $ARCH_FLAGS --prefix=%{prefix} --sysconfdir=%{sysconfdir}
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
make prefix=$RPM_BUILD_ROOT%{prefix} sysconfdir=$RPM_BUILD_ROOT%{sysconfdir} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc AUTHORS TODO README ChangeLog
%{prefix}/bin/*
%{prefix}/share/pixmaps/pkgview/*
%{prefix}/share/applications/*
%{prefix}/share/locale/*/LC_MESSAGES/*.mo
