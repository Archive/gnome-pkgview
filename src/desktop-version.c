#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libgnome/libgnome.h>
#include <config.h>
#include <string.h>
#include "desktop-version.h"

/* Parse the gnome-version.xml file to determine an overall desktop
 * version. So far, this supports platform, minor and micro versions
 * (eg. 2,0,1) and an optional vendor string which it will report in
 * parenthesis, for example:
 *
 * 2.0.1 (My Cool GNOME Distro)
 *
 * Originally adapted from my patch to gnome-about.c in cvs/gnome-desktop */

gchar * get_version_string (void) {
 	xmlDocPtr	about;
 	xmlNodePtr	node;
 	xmlNodePtr	bits;
 	gchar *		platform = "0";
 	gchar *		minor = "0";
 	gchar *		micro = "0";
 	gchar *		distro = NULL;
 
 	about = xmlParseFile ( gnome_program_locate_file (
 			       NULL, GNOME_FILE_DOMAIN_DATADIR,
 				"gnome-about/gnome-version.xml", TRUE, NULL) );
 	if (about == NULL) {
 		return _("No version data found");
 	}
 
 	node = about->children;
 	
 	if (g_strcasecmp(node->name,"gnome-version")) {
 		return _("Corrupt or incomplete version data");
 	}
 
 	bits = node->children;
 
 	while ( bits != NULL ) {
 		gchar * name = (gchar*)bits->name;
 		gchar * value = (gchar*)xmlNodeGetContent(bits);
 		if ( g_strcasecmp ( name, "platform" ) == 0)
 			platform = g_strdup ( g_strstrip(value) );
 		if ( g_strcasecmp ( name, "minor") == 0)
 			minor = g_strdup ( g_strstrip(value) );
 		if ( g_strcasecmp ( name, "micro") == 0)
 			micro = g_strdup ( g_strstrip(value) );
 		if ( (g_strcasecmp (name, "vendor")==0 && strlen(value)>0) ||		     	     (g_strcasecmp (name,"distributor")==0 && strlen(value)>0) )
 			distro = g_strdup_printf ( "(%s)", g_strstrip(value) );
 		
 		bits = bits->next;
 	}
 	xmlFreeDoc ( about );	
 	return g_strconcat(platform,".",minor,".",micro," ", distro, NULL);
}

gchar *	get_logo_filename (void) {

	if ( g_file_test("/etc/release",G_FILE_TEST_EXISTS) ) {
		gchar *release;
		g_file_get_contents ("/etc/release", &release, NULL, NULL);
		if ( g_strrstr(release,"Solaris") )
			return gnome_pixmap_file ("gnome-pkgview/solaris.png");
		g_free (release);
	}
	
	if ( g_file_test("/etc/lsb-release",G_FILE_TEST_EXISTS) ) {
		gchar *release;
		g_file_get_contents ("/etc/lsb-release", &release, NULL, NULL);
		if ( g_strrstr(release,"Ubuntu") )
			return gnome_pixmap_file ("gnome-pkgview/ubuntu.png");
		g_free (release);
	}

	if ( g_file_test("/etc/mandrake-release",G_FILE_TEST_EXISTS) ) {
                return gnome_pixmap_file ("gnome-pkgview/mandrake.png");
        }

	if ( g_file_test("/etc/fedora-release",G_FILE_TEST_EXISTS) ) {
                return gnome_pixmap_file ("gnome-pkgview/fedora.png");
        }

	if ( g_file_test("/etc/whitebox-release",G_FILE_TEST_EXISTS) ) {
		return gnome_pixmap_file ("gnome-pkgview/whitebox.png");
	}

	if ( g_file_test("/etc/redhat-release",G_FILE_TEST_EXISTS) ) {
		return gnome_pixmap_file ("gnome-pkgview/redhat.png");
	}

	if ( g_file_test("/etc/gentoo-release",G_FILE_TEST_EXISTS) ) {
                return gnome_pixmap_file ("gnome-pkgview/gentoo.png");
        }

	if ( g_file_test("/etc/debian_version",G_FILE_TEST_EXISTS) ) {
		return gnome_pixmap_file ("gnome-pkgview/debian.png");
	}

	if ( g_file_test("/etc/slackware-version",G_FILE_TEST_EXISTS) ) {
                return gnome_pixmap_file ("gnome-pkgview/slackware.png");
        }

	if ( g_file_test("/etc/SuSE-release",G_FILE_TEST_EXISTS) ) {
                return gnome_pixmap_file ("gnome-pkgview/suse.png");
        }

	if ( g_file_test("/etc/pld-release",G_FILE_TEST_EXISTS) ) {
	              return gnome_pixmap_file ("gnome-pkgview/pld.png");
        }
	
	return gnome_pixmap_file ("gnome-pkgview/gnome.png");
}
