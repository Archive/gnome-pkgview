#include <glib.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <libgnomevfs/gnome-vfs.h>
#include <gconf/gconf-client.h>
#include <config.h>
#include "pkgview.h"
#include "pkg-config.h"

gchar * strip_pc ( gchar * );
gchar * strip_nl ( gchar * );

/* use GnomeVFS to get a directory listing because its _fast_ */

void get_package_data ( gchar *dirname ) {

	GnomeVFSResult		result;
	GnomeVFSFileInfo	*info;
	GList			*list;
	GList			*node;
	GConfClient		*client;
	gboolean		show_uninst;

	gnome_vfs_init();
	
	client = gconf_client_get_default();
	show_uninst = gconf_client_get_bool
			(client,"/apps/gnome-pkgview/show_uninstalled",NULL);
	
	result = gnome_vfs_directory_list_load (&list, dirname, 0 );

	if (result != GNOME_VFS_OK) {
		return;
	}

	for (node=list; node!=NULL; node=node->next) {
		info = node->data;
		if (g_strrstr (info->name, ".pc")) {
			if ( show_uninst == FALSE &&
			     g_strrstr (info->name, "uninstalled")) {
				break;
			}
			get_file_contents (dirname,info);
			pkgs_found++;
		}
	}
	g_list_free ( list );
	g_list_free ( node );
}

/* open the file and get the bits we need to report in the UI */

void get_file_contents (gchar *path, GnomeVFSFileInfo *info) {

	FILE		*fp;
	gchar		*filename;
	gchar		temp[1024];
	gchar		prefix[1024];
	gchar		version[1024];
	gchar		desc[1024];
	gchar		target[1024];
	GtkTreeIter	iter;

	filename = g_strconcat ( path, "/", info->name, NULL);

	fp = fopen ( filename, "r" );

	while ( fgets (temp, 1023, fp) ) {
		sscanf (temp, "Version: %1023s", version);
		sscanf (temp, "Description: %1023c", desc);
		sscanf (temp, "prefix=%1023c", prefix);
		sscanf (temp, "target=%1023c", target);
	}

	fclose (fp);
	
	if (g_strrstr(desc,"${target}")) {
	    gchar *tmp;
	    gchar **find;
	    find = g_strsplit (desc, "${target}",2);
	    tmp = g_strdup_printf("%s%s%s",find[0],strip_nl(target),find[1]);
	    g_stpcpy((gchar *)&desc,tmp);
	    g_free(tmp);
	    g_strfreev(find);
	}


	/* add this to the list */
	gtk_list_store_append ( store, &iter );
	gtk_list_store_set (	store, &iter,
				0, strip_pc(info->name),
				1, version,
				2, strip_nl(prefix),
				3, strip_nl(desc),
				-1 );	

	/* make sure we don't get remnants of the last file parsed */
	memset (version, '\0', 1024);
	memset (desc, '\0', 1024);
	memset (prefix, '\0', 1024);
}	

gchar * strip_pc ( gchar *name ) {
	int i;

	for (i=0; i<strlen(name); i++) {
		if (name[i]=='.' &&
		    name[i+1] == 'p' &&
		    name[i+2] == 'c' ) {
			name[i]='\0';
		}
	}
	return g_strdup(name);
}

gchar * strip_nl ( gchar *desc_str ) {
	int i;

	for (i=0; i<strlen(desc_str); i++) {
		if (desc_str[i]=='\n') {
			desc_str[i]='\0';
		}
	}
	
	return g_strdup(desc_str);
}
