#include <glib.h>
#include <stdlib.h>
#include <gnome.h>
#include "config-path.h"
#include "pkg-config.h"
#include "pkgview.h"
#include "pkgview-cursor-utils.h"

void parse_pkgconfig_path ( void ) {

	gchar *default_path;
	gchar *path = NULL;
	int dirs = 0;

	pkgs_found = 0;

	/* set the busy cursor */

	pkgview_set_busy_cursor (window, TRUE);
      
       	/* parse the PKG_CONFIG_PATH env var, and add a terminator */
	if ( getenv ("PKG_CONFIG_PATH") ) {
		path = g_strconcat(getenv("PKG_CONFIG_PATH"),":DIRS_DONE",NULL);
	}

	/* walk the PKG_CONFIG_PATH for other files */
	if ( path ) {
		gchar **directories;
		int node=0;
		directories = g_strsplit ( path, ":", 0);;
		
		while ( ! g_strrstr( directories[node], "DIRS_DONE" ) ) {
			get_package_data( directories[node] );
			node++;
			dirs++;
		}

		g_strfreev ( directories );
	}

	if ( dirs == 0) {
	        default_path = gnome_program_locate_file ( pkgview,
			    GNOME_FILE_DOMAIN_LIBDIR, "pkgconfig", 1, FALSE);
		g_print("%s\n",default_path);
		get_package_data (default_path);
	}

	/* unset the busy cursor */

	pkgview_set_busy_cursor (window, FALSE);

}

