/*  Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA. */

#include <config.h>
#include <gnome.h>
#include <gtk/gtk.h>
#include "pkgview.h"
#include "pkg-config.h"
#include "config-path.h"
#include "desktop-version.h"
#include "about-box.h"
 
/* generic close window/destroy callback */

void app_exit ( void ) {

	gtk_main_quit ();
	
}
	
/* set up the main window, its name, icon etc. */

void create_main_window ( void ) {

	GdkPixbuf	*icon;
	icon = gdk_pixbuf_new_from_file ( 
		gnome_pixmap_file ("gnome-pkgview.png"), NULL );
	gtk_window_set_title ( GTK_WINDOW ( window ),
			      _("Desktop Library Version Inspector") );
	gtk_window_set_icon ( GTK_WINDOW ( window ), icon );
	g_object_unref ( G_OBJECT (icon) );
	gtk_container_add ( GTK_CONTAINER ( window ), view);

	if (pkgs_found) {
		gtk_window_set_default_size ( GTK_WINDOW ( window ),
					      450, 350 );
	}
	
	g_signal_connect ( G_OBJECT ( window ), "delete-event",
			   G_CALLBACK ( app_exit ), NULL );

        g_signal_connect ( G_OBJECT ( window ), "destroy",
			   G_CALLBACK ( app_exit ), NULL ); 
	
}

/* return the box with the close button */

GtkWidget * create_control_box ( void ) {

	GtkWidget	*hbox;
	GtkWidget	*about_bttn;
	GtkWidget	*refresh_bttn;
	AtkObject	*obj;

	hbox = gtk_hbox_new ( FALSE, 0 );
	
	close_bttn = gtk_button_new_from_stock ( GTK_STOCK_CLOSE );
	obj = gtk_widget_get_accessible (GTK_WIDGET(close_bttn));
	atk_object_set_description (obj,_("Closes the window"));
	
	about_bttn = gtk_button_new_from_stock ( GNOME_STOCK_ABOUT );
        obj = gtk_widget_get_accessible (GTK_WIDGET(about_bttn));
	atk_object_set_description (obj,_("Displays the about dialog"));
	
	refresh_bttn = gtk_button_new_from_stock ( GTK_STOCK_REFRESH );
        obj = gtk_widget_get_accessible (GTK_WIDGET(refresh_bttn));
	atk_object_set_description (obj,_("Refreshes the package list"));
	
	gtk_box_pack_end ( GTK_BOX(hbox), close_bttn, FALSE, FALSE, 4);
	gtk_box_pack_end ( GTK_BOX(hbox), refresh_bttn, FALSE, FALSE, 4);
	gtk_box_pack_start ( GTK_BOX(hbox), about_bttn, FALSE, FALSE, 4);

	g_signal_connect ( G_OBJECT(close_bttn), "clicked", 
			   G_CALLBACK (app_exit), NULL);

	g_signal_connect ( G_OBJECT(refresh_bttn), "clicked",
			   G_CALLBACK (refresh_pkgconfig_list), NULL);

	g_signal_connect ( G_OBJECT(about_bttn), "clicked",
			   G_CALLBACK (create_about_box), NULL);

	return hbox;
}

/* return a TreeModel for our package list */

GtkTreeModel * create_list_model ( void ) {
	
	store = gtk_list_store_new ( 4, G_TYPE_STRING,
					G_TYPE_STRING,
					G_TYPE_STRING,
					G_TYPE_STRING);

	return GTK_TREE_MODEL( store );

}

/* utility function to return a column to hold strings */

GtkTreeViewColumn * create_column ( gint number, gchar *title ) {

	GtkTreeViewColumn	*column;
	GtkCellRenderer		*renderer;

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (
			title, renderer, "text", number, NULL );
	gtk_tree_view_column_set_sort_column_id ( column, number );
	
	return column;
}

/* return the box to hold the gnome-desktop version */

GtkWidget * create_version_label ( void ) {

	GtkWidget		*label;
	GtkWidget		*box;
	GtkWidget		*logo;
	gchar			*version;
	AtkObject		*obj;

	box = gtk_hbox_new ( FALSE, 0 );
	logo = gtk_image_new_from_file (get_logo_filename());
	obj = gtk_widget_get_accessible (GTK_WIDGET(logo));
	atk_object_set_name (obj,_("Vendor/Distribution logo"));
	atk_image_set_image_description
		(ATK_IMAGE(obj),_("Vendor/Distribution logo"));
	
	version = g_strdup_printf (_("Reported GNOME Desktop Version: %s"),
				   get_version_string() );
	label = gtk_label_new ( version );
	obj = gtk_widget_get_accessible (GTK_WIDGET(label));
	atk_object_set_description
		(obj,_("The version of GNOME you are running"));
	atk_object_set_name (obj,_("Desktop version string"));
	gtk_label_set_selectable ( GTK_LABEL(label), TRUE );
	gtk_box_pack_start ( GTK_BOX ( box ), logo, FALSE, FALSE, 4 );
	gtk_box_pack_start ( GTK_BOX ( box ), label, TRUE, FALSE, 4 );
	g_free ( version );
	return box;
	
}

/* return the main window contents, including the list and associated
 * scrolled window stuff */

GtkWidget * create_list_view ( void ) {

	GtkWidget		*scrolled;
	GtkWidget		*list_hbox;
	GtkWidget		*version_hbox;
	GtkWidget		*main_vbox;
	GtkWidget		*control_hbox;
	GtkWidget		*hsep;
	GtkTreeViewColumn	*column;
	AtkObject		*obj;
	
	list_hbox = gtk_hbox_new ( TRUE, 0 );
	main_vbox = gtk_vbox_new ( FALSE, 0 );
	scrolled = gtk_scrolled_window_new ( NULL, NULL );
	gtk_scrolled_window_set_policy ( GTK_SCROLLED_WINDOW ( scrolled ),
					 GTK_POLICY_AUTOMATIC,
					 GTK_POLICY_AUTOMATIC);
	tree_view = GTK_TREE_VIEW(gtk_tree_view_new_with_model ( GTK_TREE_MODEL ( model ) ));
	obj = gtk_widget_get_accessible(GTK_WIDGET(tree_view));
	atk_object_set_name(obj,_("Package List"));
	atk_object_set_description(obj,_("List of installed GNOME components"));
	
	gtk_container_add ( GTK_CONTAINER ( scrolled ), 
			    GTK_WIDGET ( tree_view ) );
	gtk_box_pack_start ( GTK_BOX ( list_hbox ), scrolled, TRUE, TRUE, 3);
	gtk_tree_view_set_rules_hint ( GTK_TREE_VIEW ( tree_view ), TRUE );

	column = create_column ( 0, _("Package Name") );
	gtk_tree_view_append_column ( tree_view, column );

        column = create_column ( 1, _("Version") );
        gtk_tree_view_append_column ( tree_view, column );

	column = create_column ( 2, _("Prefix") );
	gtk_tree_view_append_column ( tree_view, column );
	
        column = create_column ( 3, _("Description") );
        gtk_tree_view_append_column ( tree_view, column );
	
	gtk_tree_sortable_set_sort_column_id ( GTK_TREE_SORTABLE ( model ),
					       0, GTK_SORT_ASCENDING);

	version_hbox = create_version_label ();
	control_hbox = create_control_box ();
	hsep = gtk_hseparator_new ();
	gtk_box_pack_start ( GTK_BOX (main_vbox), version_hbox, FALSE, TRUE, 4);

	/* if no .pc files are installed, don't show an empty list */
	if (pkgs_found) {
		gtk_box_pack_start ( GTK_BOX (main_vbox), list_hbox, 
				     TRUE, TRUE, 4 );
	}
	gtk_box_pack_start ( GTK_BOX (main_vbox), hsep, FALSE, FALSE, 0);
	gtk_box_pack_start ( GTK_BOX (main_vbox), control_hbox, FALSE, TRUE, 4);

	
	return main_vbox;
}

/* clear store saving packages list and reload pkgconfig data */

void refresh_pkgconfig_list(void)
{
  gtk_list_store_clear(store);
  parse_pkgconfig_path();
}

int main ( int argc, char *argv[] ) {

	setlocale(LC_ALL,"");
	bindtextdomain ( GETTEXT_PACKAGE, GNOMELOCALEDIR );
	bind_textdomain_codeset ( GETTEXT_PACKAGE, "UTF-8" );
	textdomain ( GETTEXT_PACKAGE );

	pkgview = gnome_program_init ( "gnome-pkgview", VERSION, 
					LIBGNOMEUI_MODULE ,argc, argv, NULL );
	
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	model = create_list_model ();
	parse_pkgconfig_path();
	view = create_list_view ();
	create_main_window();
	
	gtk_widget_show_all ( GTK_WIDGET(window) );
	gtk_main();
	
	return 0;
}

