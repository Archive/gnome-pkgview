#include <config.h>
#include "pkgview.h"
#include "pkgview-cursor-utils.h"

void pkgview_set_busy_cursor (GtkWidget *window, gboolean state) {

  GdkCursor *cursor = NULL;
  if (!GTK_WIDGET_REALIZED(window)) return;

  if (state) {
	  cursor = gdk_cursor_new (GDK_WATCH);
	  gdk_window_set_cursor (GDK_WINDOW(window->window), cursor);
  } else {
	  gdk_window_set_cursor (GDK_WINDOW(window->window), NULL);
  }

  if (cursor) gdk_cursor_unref (cursor);

  /* can't use gdk_display_flush because it would leave us incompatible
     with earlier versions of gtk-2.x */

  gdk_display_sync (gtk_widget_get_display (GTK_WIDGET(window)));

}
