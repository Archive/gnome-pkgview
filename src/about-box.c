#include <gnome.h>
#include <config.h>
#include "about-box.h"

void create_about_box ( void ) {

        GdkPixbuf *pixbuf;
	gchar *pic = gnome_pixmap_file("gnome-pkgview.png");

	/* Don't create more than one about box */
	if (about_dialog != NULL) {
		g_assert (GTK_WIDGET_REALIZED (about_dialog));
		gdk_window_show (about_dialog->window);
		gdk_window_raise (about_dialog->window);
	} else {
	 	const gchar *trans = _("Untranslated Version");
		const gchar *authors[] = {
			"Mike Newman <mikegtn@gnome.org>\n",
			"with invaluable assistance from: \n",
			"Jeff Waugh <jdub@perkypants.org>",
			"Norihiro Umeda <Norihiro.Umeda@is.titech.ac.jp>",
			NULL};
			pixbuf = gdk_pixbuf_new_from_file ( pic, NULL );
		about_dialog = gnome_about_new (
			_("Desktop Library Version Inspector"),
			VERSION, "© 2002-2006 Mike Newman",
			_("An application to determine the version "
			"of GNOME components on your system\n\n"
			"Released under the terms of the GNU GPL"),
			authors, NULL, trans, pixbuf);
		g_signal_connect (G_OBJECT( about_dialog ), "destroy",
			G_CALLBACK (gtk_widget_destroyed), &about_dialog);
		g_object_unref (G_OBJECT(pixbuf));
		gtk_widget_show (about_dialog);
	}
	
	g_free(pic);
}
