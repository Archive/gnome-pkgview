#include <config.h>
#include <gnome.h>
#include <gtk/gtk.h>

GtkWidget       *window;
GtkListStore    *store;
GtkTreeModel    *model;
GtkTreeView	*tree_view;
GtkWidget       *view;
GtkWidget	*working;
GtkWidget	*close_bttn;
GnomeProgram	*pkgview;
int		pkgs_found;

void app_exit ( void );
void more_bttn_cb ( void );
void create_main_window ( void );
void refresh_pkgconfig_list ( void );
GtkTreeModel * create_list_model ( void );
GtkTreeViewColumn * create_column ( gint, gchar * );
GtkWidget * create_version_label ( void );
GtkWidget * create_list_view ( void );
GtkWidget * create_control_box ( void );
gchar * get_logo_filename ( void );
